package abdlh.com.billapp;

/**
 * Created by abdellah on 03/01/15.
 */
public class Card {

    private String centerNumber, bottomNumber, imageUrl, bankName;

    public String getCenterNumber() {
        return centerNumber;
    }

    public void setCenterNumber(String centerNumber) {
        this.centerNumber = centerNumber;
    }

    public String getBottomNumber() {
        return bottomNumber;
    }

    public void setBottomNumber(String bottomNumber) {
        this.bottomNumber = bottomNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
