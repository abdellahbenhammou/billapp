package abdlh.com.billapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class PreviewDataActivity extends Activity {
    //static byte[] imageToShow=null;
    Button newcard, savedcard;
    EditText amount, destination, date;
    TextView chosenCardNumber;
    Button pay;
    Context context;
    String chosenCard="";
    public int pos=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_data);
        chosenCardNumber = (TextView) findViewById(R.id.chosenCard);
        pay = (Button) findViewById(R.id.pay);
        context = this;
        //EditText centerNumber, bottomNumber, date_new;
        amount = (EditText) findViewById(R.id.amount);
        destination = (EditText) findViewById(R.id.destination);
        date = (EditText) findViewById(R.id.date);

        newcard = (Button) findViewById(R.id.newcard);
        savedcard = (Button) findViewById(R.id.savedcard);

        showLoading("Processing scan...");
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                amount.setText("181.00");
                destination.setText("Pizza Hut");
                date.setText("07/01/2015");
            }}, 3000);

        newcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog newcard_dialog = new Dialog(context);
                newcard_dialog.setTitle("Add new card");
                newcard_dialog.setContentView(R.layout.dialog_add_card);
                Button add_new = (Button) newcard_dialog.findViewById(R.id.add_new);
                final EditText centerNumber = (EditText) newcard_dialog.findViewById(R.id.centerNumber);
                final EditText bottomNumber = (EditText) newcard_dialog.findViewById(R.id.bottomNumber);
                final EditText date_new = (EditText)newcard_dialog.findViewById(R.id.date);

                date_new.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                        // TODO Auto-generated method stub
                        if (date_new.getText().length() == 2 && pos != 3) {
                            date_new.setText(date_new.getText().toString() + "/");
                            date_new.setSelection(3);
                        }
                    }
                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                        // TODO Auto-generated method stub
                        pos = date_new.getText().length();
                    }
                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                    }
                });
                add_new.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Card card = new Card();
                        card.setBankName("Finance Bank");
                        card.setCenterNumber(centerNumber.getText().toString());
                        card.setBottomNumber(bottomNumber.getText().toString());
                        card.setImageUrl("www.google.com");
                        if(centerNumber.getText().length() == 0
                                || bottomNumber.getText().length() == 0
                                || centerNumber.getText().length() < 13
                                || bottomNumber.getText().length() <6 )
                            Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                        else if(utils.writeNewCards(getApplicationContext(), card)) {
                            newcard_dialog.dismiss();
                            chosenCard = centerNumber.getText().toString();
                            chosenCardNumber.setText("Card: " + chosenCard);
                            pay.setVisibility(View.VISIBLE);
                            //chosenCardNumber.setVisibility(View.VISIBLE);
                        }
                        else
                            Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                    }
                });
                newcard_dialog.show();
            }
        });

        savedcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showLoading("Please wait...");
                Intent i = new Intent(context, ViewCardsActivity.class);
                i.putExtra("source", 1);
                startActivityForResult(i, 1);
            }
        });
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog dialog  = new ProgressDialog(context);;

                dialog.setMessage("Please wait...");
                dialog.show();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        dialog.dismiss();
                        Toast.makeText(context, "Payment successful", Toast.LENGTH_SHORT).show();
                        finish();
                    }}, 3000);
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                chosenCard =data.getStringExtra("result");
                chosenCardNumber.setText("Card: " + chosenCard);
                pay.setVisibility(View.VISIBLE);

            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
                chosenCardNumber.setText("Card: None");
                pay.setVisibility(View.INVISIBLE);
            }
        }
    }
    public void showLoading(String msg){
        final ProgressDialog dialog  = new ProgressDialog(context);;

        dialog.setMessage(msg);
        dialog.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialog.dismiss();
            }}, 3000);
    }
}
