package abdlh.com.billapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by abdellah on 03/01/15.
 */
public class CardsAdapter extends BaseAdapter {
    List<Card> cardsList;
    Context context;
    ViewHolder viewHolder;
    public CardsAdapter(List<Card> list, Context context){
        this.cardsList = list;
        this.context = context;
    }
    @Override
    public int getCount() {
        return cardsList.size();
    }

    @Override
    public Object getItem(int position) {
        return cardsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.card_row, parent,false);
            viewHolder = new ViewHolder();
            viewHolder.bankName = (TextView) convertView.findViewById(R.id.bankName);
            viewHolder.centerNumber = (TextView) convertView.findViewById(R.id.centerNumber);
            viewHolder.bottomNumber = (TextView) convertView.findViewById(R.id.bottomNumber);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.bankLogo);
            viewHolder.dataContainer = (LinearLayout) convertView.findViewById(R.id.data_container);
            convertView.setTag(viewHolder);
        }
        else
            viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.bankName.setText(cardsList.get(position).getBankName());
        viewHolder.centerNumber.setText(cardsList.get(position).getCenterNumber());
        viewHolder.bottomNumber.setText(cardsList.get(position).getBottomNumber());
        if(position % 2 == 0) {
            viewHolder.image.setImageResource(R.drawable.citibank128);
            viewHolder.dataContainer.setBackgroundColor(Color.parseColor("#023374"));
        }
        else{
            //FF02765B
            viewHolder.image.setImageResource(R.drawable.paribas);
            viewHolder.dataContainer.setBackgroundColor(Color.parseColor("#02765B"));
        }

        return convertView;
    }
    static class ViewHolder{
        TextView bankName;
        TextView centerNumber;
        TextView bottomNumber;
        ImageView image;
        LinearLayout dataContainer;
    }
}
