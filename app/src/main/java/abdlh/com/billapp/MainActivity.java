package abdlh.com.billapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MainActivity extends SherlockFragmentActivity {
    //private MainFragment mainFragment;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
/*
        Session session = Session.getActiveSession();
        if(!(session != null && session.isOpened())) {
            //getSupportFragmentManager().beginTransaction().replace(R.id.content,  new ViewCardsFragment()).commit();
            startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }*/
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(position == 0) {
                    getActionBar().setTitle("New Bill");
                    getActionBar().hide();
                }
                else if(position ==1) {
                    getActionBar().setTitle("Credit Cards");
                    getActionBar().show();
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        String firstName = utils.readFirstName(this);
        if(firstName == null) {
            new Request(
                    Session.openActiveSessionFromCache(this),
                    "/me",
                    null,
                    HttpMethod.GET,
                    new Request.Callback() {
                        public void onCompleted(Response response) {
            /* handle the result */
                            try {
                                Log.d("BillApp", "The response: " + response.getGraphObject().asMap().toString());
                                Toast.makeText(getApplication(), "Welcome " + new JSONObject(response.getGraphObject().asMap()).getString("first_name") + "!", Toast.LENGTH_SHORT).show();
                                utils.writeFirstName(getApplicationContext(), new JSONObject(response.getGraphObject().asMap()).getString("first_name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAsync();
        }
        else
            Toast.makeText(getApplication(), "Welcome " + firstName +"!", Toast.LENGTH_SHORT).show();
    }

    public void onStart(){
        super.onStart();
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "abdlh.com.billapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        //getSupportFragmentManager().beginTransaction().replace(R.id.content, new MainFragment()).commit();
    }
    @Override
    public void onResume(){
        super.onResume();


    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:

                    return ScanFragment.newInstance(false);
                case 1:

                    return ViewCardsFragment.newInstance();
                default:

                    return ScanFragment.newInstance(false);
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0)
                return "Credit Cards";
            else if (position == 1)
                return "Scan";
            return "Error";
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // ²Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_new_card) {
            startActivity(new Intent(this, AddCardActivity.class));
            return true;
        }
        else if(id == R.id.action_logout){
            if (Session.getActiveSession() != null) {
                Session.getActiveSession().closeAndClearTokenInformation();
                // Session.getActiveSession().close();
                Log.i("BillApp", "logging out using menu action");
                Session.setActiveSession(null);
                startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();

            }
        }

        return super.onOptionsItemSelected(item);
    }
}
