package abdlh.com.billapp;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;


public class AddCardActivity extends Activity {
    EditText centerNumber, bottomNumber, date;
    public int pos=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        Button add_new = (Button) findViewById(R.id.add_new);
        //bankName = (EditText) findViewById(R.id.bankName);
        centerNumber = (EditText) findViewById(R.id.centerNumber);
        bottomNumber = (EditText) findViewById(R.id.bottomNumber);
        date = (EditText)findViewById(R.id.date);
        date.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                if (date.getText().length() == 2 && pos != 3) {
                    date.setText(date.getText().toString() + "/");
                    date.setSelection(3);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                pos = date.getText().length();
            }
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        add_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Card card = new Card();
                card.setBankName("Finance Bank");
                card.setCenterNumber(centerNumber.getText().toString());
                card.setBottomNumber(bottomNumber.getText().toString());
                card.setImageUrl("www.google.com");
                if(centerNumber.getText().length() == 0
                        || bottomNumber.getText().length() == 0
                        || centerNumber.getText().length() < 13
                        || bottomNumber.getText().length() <6 )
                    Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                else if(utils.writeNewCards(getApplicationContext(), card))
                    finish();
                else
                    Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void onStart(){
        super.onStart();
        //Filling up the spinner of countries:
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for( Locale loc : locale ){
            country = loc.getDisplayCountry();
            if( country.length() > 0 && !countries.contains(country) ){
                countries.add( country );
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        Spinner citizenship = (Spinner)findViewById(R.id.countries_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_item, countries);
        citizenship.setAdapter(adapter);
        //------------------------------
    }
}
