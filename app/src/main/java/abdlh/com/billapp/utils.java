package abdlh.com.billapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by abdellah on 03/01/15.
 */
public class utils {
    public static String TAG = "BillApp";
    public static ArrayList<Card> readCards(Context context){
        SharedPreferences prefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        String value = prefs.getString("cardsList", null);
        Card[] list = null;
        if(value!=null) {
            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb.create();
            list = gson.fromJson(value, Card[].class);
        }
        else
            list = new Card[0];


        ArrayList<Card> cardsList = new ArrayList<>(Arrays.asList(list));
        return cardsList;
    }



    public static Boolean writeNewCards(Context context, Card card){
        SharedPreferences prefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        ArrayList<Card> list = new ArrayList<>();
        list.addAll(readCards(context));
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        //adding the new card
        Log.d(TAG, "The new card: " + card.getBankName());
        /*Card card1 = new Card();
        card1.setBankName("Citi Bank");
        card1.setImageUrl("www.google.com");
        card1.setBottomNumber("12345");
        card1.setCenterNumber("102030405060");*/

        list.add(card);
        String value = gson.toJson(list);

        SharedPreferences.Editor e = prefs.edit();
        e.putString("cardsList", value);
        e.commit();
        return true;

    }
    public static Boolean writeCards(Context context, ArrayList<Card> new_list){
        SharedPreferences prefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        ArrayList<Card> list = new ArrayList<>();
        list.addAll(new_list);
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        String value = gson.toJson(list);
        SharedPreferences.Editor e = prefs.edit();
        e.putString("cardsList", value);
        e.commit();
        return true;

    }

    public static String readFirstName(Context context){
        SharedPreferences prefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE);

        return prefs.getString("first_name", null);
    }

    public static void writeFirstName(Context context, String firstName){
        SharedPreferences prefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor e = prefs.edit();
        e.putString("first_name", firstName);
        e.commit();
    }
}
