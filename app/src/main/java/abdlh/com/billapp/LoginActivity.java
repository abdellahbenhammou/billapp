package abdlh.com.billapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.facebook.Session;


public class LoginActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Session session = Session.getActiveSession() ;
        if(session == null)
            session = Session.openActiveSessionFromCache(this);
        if(session != null && session.isOpened()) {
            //getSupportFragmentManager().beginTransaction().replace(R.id.content,  new ViewCardsFragment()).commit();
            startActivity(new Intent(this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
        else
            getSupportFragmentManager().beginTransaction().replace(R.id.content, new LoginFragment()).commit();
    }
}
