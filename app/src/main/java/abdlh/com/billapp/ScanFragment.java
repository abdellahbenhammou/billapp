package abdlh.com.billapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by abdellah on 06/01/15.
 */
import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.Parameters;

import android.os.SystemClock;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;
import com.commonsware.cwac.camera.CameraHost;
import com.commonsware.cwac.camera.CameraUtils;
import com.commonsware.cwac.camera.SimpleCameraHost;
import com.commonsware.cwac.camera.PictureTransaction;

import com.commonsware.cwac.camera.acl.CameraFragment;
public class ScanFragment extends CameraFragment{
    //View view;
    private MenuItem singleShotItem=null;
    private MenuItem autoFocusItem=null;
    private MenuItem takePictureItem=null;
    private MenuItem mirrorFFC=null;
    private static final String KEY_USE_FFC=
            "com.commonsware.cwac.camera.demo.USE_FFC";

    private boolean singleShotProcessing=false;
    private long lastFaceToast=0L;
    String flashMode=null;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.scan_layout, container, false);
        context = getActivity();
        View cameraView=
                super.onCreateView(inflater, container, savedInstanceState);
        View results=inflater.inflate(R.layout.scan_layout, container, false);
        ((ViewGroup)results.findViewById(R.id.camera)).addView(cameraView);
        final ImageButton take_picture = (ImageButton) results.findViewById(R.id.take_picture);
        take_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
                startActivity(new Intent(context, PreviewDataActivity.class));
            }
        });
        //zoom.setKeepScreenOn(true);
        //setRecordingItemVisibility();
        return(results);
        //return view;
    }
    static ScanFragment newInstance(boolean useFFC) {
        ScanFragment f=new ScanFragment();
        Bundle args=new Bundle();
        args.putBoolean(KEY_USE_FFC, useFFC);
        f.setArguments(args);
        return(f);
    }
    boolean isSingleShotProcessing() {
        return(singleShotProcessing);
    }

    public void onCreate(Bundle state){
        super.onCreate(state);
        setHasOptionsMenu(true);
        SimpleCameraHost.Builder builder=
                new SimpleCameraHost.Builder(new DemoCameraHost(getActivity()));
        setHost(builder.useFullBleedPreview(true).build());


    }

    void takeSimplePicture() {
        /*if (singleShotItem!=null && singleShotItem.isChecked()) {
            singleShotProcessing=true;
            takePictureItem.setEnabled(false);
        }
        PictureTransaction xact=new PictureTransaction(getHost());
        if (flashItem!=null && flashItem.isChecked()) {
            xact.flashMode(flashMode);
        }*/
        takePicture();
    }

    class DemoCameraHost extends SimpleCameraHost implements
            Camera.FaceDetectionListener {
        boolean supportsFaces=false;
        public DemoCameraHost(Context _ctxt) {
            super(_ctxt);
        }
        @Override
        public boolean useFrontFacingCamera() {
            if (getArguments() == null) {
                return(false);
            }
            return(getArguments().getBoolean(KEY_USE_FFC));
        }
        @Override
        public boolean useSingleShotMode() {
            if (singleShotItem == null) {
                return(false);
            }
            return(singleShotItem.isChecked());
        }
        @Override
        public void saveImage(PictureTransaction xact, byte[] image) {
            if (useSingleShotMode()) {
                singleShotProcessing=false;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        takePictureItem.setEnabled(true);
                    }
                });
                //DisplayActivity.imageToShow=image;
                //startActivity(new Intent(getActivity(), DisplayActivity.class));
            }
            else {
                super.saveImage(xact, image);
            }
        }
        @Override
        public void autoFocusAvailable() {
            if (autoFocusItem != null) {
                autoFocusItem.setEnabled(true);
                if (supportsFaces)
                    startFaceDetection();
            }
        }
        @Override
        public void autoFocusUnavailable() {
            if (autoFocusItem != null) {
                stopFaceDetection();
                if (supportsFaces)
                    autoFocusItem.setEnabled(false);
            }
        }
        @Override
        public void onCameraFail(CameraHost.FailureReason reason) {
            super.onCameraFail(reason);
            Toast.makeText(getActivity(),
                    "Sorry, but you cannot use the camera now!",
                    Toast.LENGTH_LONG).show();
        }
        @Override
        public Parameters adjustPreviewParameters(Parameters parameters) {
            flashMode=
                    CameraUtils.findBestFlashModeMatch(parameters,
                            Camera.Parameters.FLASH_MODE_RED_EYE,
                            Camera.Parameters.FLASH_MODE_AUTO,
                            Camera.Parameters.FLASH_MODE_ON);
            if (doesZoomReallyWork() && parameters.getMaxZoom() > 0) {
                //zoom.setMax(parameters.getMaxZoom());
                //zoom.setOnSeekBarChangeListener(DemoCameraFragment.this);
            }
            else {
                //zoom.setEnabled(false);
            }
            if (parameters.getMaxNumDetectedFaces() > 0) {
                supportsFaces=true;
            }
            else {
                Toast.makeText(getActivity(),
                        "Face detection not available for this camera",
                        Toast.LENGTH_LONG).show();
            }
            return(super.adjustPreviewParameters(parameters));
        }
        @Override
        public void onFaceDetection(Face[] faces, Camera camera) {
            if (faces.length > 0) {
                long now=SystemClock.elapsedRealtime();
                if (now > lastFaceToast + 10000) {
                    Toast.makeText(getActivity(), "I see your face!",
                            Toast.LENGTH_LONG).show();
                    lastFaceToast=now;
                }
            }
        }
        @Override
        @TargetApi(16)
        public void onAutoFocus(boolean success, Camera camera) {
            super.onAutoFocus(success, camera);
            takePictureItem.setEnabled(true);
        }
        @Override
        public boolean mirrorFFC() {
            return(mirrorFFC.isChecked());
        }
    }
}

