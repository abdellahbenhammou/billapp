package abdlh.com.billapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by abdellah on 02/01/15.
 */
public class ViewCardsFragment extends Fragment{
    ArrayList<Card> cardsList;
    CardsAdapter cardsAdapter;
    ListView listView;
    Button addCard;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.viewcards_layout, container, false);
        return view;
    }
    public static ViewCardsFragment newInstance() {
        ViewCardsFragment f = new ViewCardsFragment();
        return f;
    }

    public void onStart(){
        super.onStart();
        cardsList = new ArrayList<Card>();


    }
    public void onResume(){
        super.onResume();

        cardsList = utils.readCards(getActivity());
        cardsAdapter = new CardsAdapter(cardsList, getActivity());
        listView = (ListView) view.findViewById(R.id.list);
        listView.setEmptyView(view.findViewById(R.id.empty));
        listView.setAdapter(cardsAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(getActivity()).setMessage("Delete Card ?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ArrayList<Card> list = new ArrayList<Card>();
                        list.addAll(utils.readCards(getActivity()));
                        list.remove(position);
                        utils.writeCards(getActivity(), list);

                        cardsAdapter.cardsList.remove(position);
                        cardsAdapter.notifyDataSetChanged();
                    }
                }).setNegativeButton("No", null).create().show();
                return false;
            }
        });
        if(getActivity().getIntent().getIntExtra("source", 0) == 1){
            Log.d("BillApp", "The onclick being set...");
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", cardsAdapter.cardsList.get(position).getCenterNumber());
                    getActivity().setResult(Activity.RESULT_OK, returnIntent);
                    getActivity().finish();
                    Log.d("BillApp", "viewCards finished..");
                }
            });
        }
    }
}
